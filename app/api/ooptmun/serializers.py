from rest_framework import serializers
from api.models import OOPTMun


class OOPTMunSerializer(serializers.ModelSerializer):
    class Meta:
        model = OOPTMun
        fields = ('mun_id',
                  'stats',
                  )