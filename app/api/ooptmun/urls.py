from django.urls import path
from .views import  OOPTMunList

urlpatterns = [
    path('', OOPTMunList.as_view()),
]