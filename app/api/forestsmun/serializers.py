from rest_framework import serializers
from api.models import ForestsMun


class ForestsMunSerializer(serializers.ModelSerializer):
    class Meta:
        model = ForestsMun
        fields = ('mun_id',
                  'stats',
                  )