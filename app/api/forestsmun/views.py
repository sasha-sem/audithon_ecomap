from django.shortcuts import render
from rest_framework import generics, status
from api.models import ForestsMun
from .serializers import ForestsMunSerializer
from rest_framework.response import Response


class ForestsMunList(generics.ListAPIView):
    queryset = ForestsMun.objects.all()
    serializer_class = ForestsMunSerializer
    def get(self, request):
        param_handler = self.request.query_params
        year = param_handler.get('year')
        munic = param_handler.get('munic')
        result = {"error":"incorrect input"}
        if munic and year:
            try:
                data = ForestsMun.objects.filter(mun=munic).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Лесной покров занимает более 5% муниципалитета",
                "units":"Да/Нет",
                "year":year,
                "mun_id":data.mun_id,
                "mun_name":data.mun.name,
                "reg_name":data.mun.region,
                "value":data.stats.get(str(year), "")
            }
        elif munic:
            try:
                data = ForestsMun.objects.filter(mun=munic).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Лесной покров занимает более 5% муниципалитета",
                "units":"Да/Нет",
                "mun_id":data.mun_id,
                "mun_name":data.mun.name,
                "reg_name":data.mun.region,
                "stats": {
                    "years":data.stats.keys(),
                    "values": data.stats.values()
                }
            }
        elif year:
            data = ForestsMun.objects.all()
            result = {
                "title":"Лесной покров занимает более 5% муниципалитета",
                "units":"Да/Нет",
                "year":year,
                "regions":[]
            }
            for it in data:
                result["regions"].append({"mun_id":it.mun_id,"mun_name":it.mun.name, "reg_name":it.mun.region,  "value":it.stats.get(str(year), "")})

        return Response(result)