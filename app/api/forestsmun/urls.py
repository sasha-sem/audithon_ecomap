from django.urls import path
from .views import  ForestsMunList

urlpatterns = [
    path('', ForestsMunList.as_view()),
]