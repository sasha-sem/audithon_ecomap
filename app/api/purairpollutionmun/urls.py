from django.urls import path
from .views import  PurAirPollutionMunList

urlpatterns = [
    path('', PurAirPollutionMunList.as_view()),
]