from rest_framework import serializers
from api.models import PurAirPollutionMun


class PurAirPollutionMunSerializer(serializers.ModelSerializer):
    class Meta:
        model = PurAirPollutionMun
        fields = ('mun_id',
                  'stats',
                  )