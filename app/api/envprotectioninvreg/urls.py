from django.urls import path
from .views import EnvProtectionInvRegList

urlpatterns = [
    path('', EnvProtectionInvRegList.as_view()),
]