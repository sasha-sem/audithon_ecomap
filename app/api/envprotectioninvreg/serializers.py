from rest_framework import serializers
from api.models import EnvProtectionInvReg


class EnvProtectionInvRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnvProtectionInvReg
        fields = ('reg_id',
                  'stats',
                  )