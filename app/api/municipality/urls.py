from django.urls import path
from .views import MunicipalityDetail, MunicipalityList

urlpatterns = [
    path('<int:pk>/', MunicipalityDetail.as_view()),
    path('', MunicipalityList.as_view()),
]