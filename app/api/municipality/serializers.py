from rest_framework import serializers
from api.models import Municipality


class MunicipalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Municipality
        fields = '__all__'


class MunicipalityDetailSerializer(serializers.ModelSerializer):
    plots = serializers.SerializerMethodField()
    def get_plots(self, instance):
        result = []
        ecoriskent = {
            'ecoriskent':
            {'title': "Экологически рискованные предприятия",
                  "units": "ед.",
                  "stats": {
                      "years": instance.ecoriskentmun.stats.keys(),
                      "values": instance.ecoriskentmun.stats.values()
                  }}}
        purairpollution = {'purairpollution':{'title': "Очищенные загрязнения в воздухе", "units": "тыс. тонн",
                  "stats": {
                      "years": instance.purairpollutionmun.stats.keys(),
                      "values": instance.purairpollutionmun.stats.values()
                  }
                  }}
        forests = {'forests':{'title': "Лесной покров занимает более 5% муниципалитета", "units": "Да/Нет",
                  "stats": {
                      "years": instance.forestsmun.stats.keys(),
                      "values": instance.forestsmun.stats.values()
                  }
                  }}
        oopt = {'oopt':{'title': "Количество ООПТ в муниципалитете", "units": "ед.",
                  "stats": {
                      "years": instance.ooptmun.stats.keys(),
                      "values": instance.ooptmun.stats.values()
                  }
                  }}
        airpollution = {'airpollution':{'title': "Загрязняющие вещества в воздухе", "units": "тыс. тонн",
                  "stats": {
                      "years": instance.airpollutionmun.stats.keys(),
                      "values": instance.airpollutionmun.stats.values()
                  }
                  }}
        contaminants = {'contaminants':{'title': "Загрязняющие вещества", "units": "тыс. тонн",
                  "stats": {
                      "years": instance.contaminantsmun.stats.keys(),
                      "values": instance.contaminantsmun.stats.values()
                  }
                  }}
        result = [ecoriskent, purairpollution,forests, oopt,airpollution, contaminants]
        return result

    class Meta:
        model = Municipality
        fields = ('id',
                  'name',
                  'region',
                  'plots',
                  'extent',
                  )
