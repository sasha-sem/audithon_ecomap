from django.shortcuts import render
from rest_framework import generics, status
from api.models import Municipality
from .serializers import MunicipalitySerializer, MunicipalityDetailSerializer
# Create your views here.

class MunicipalityDetail(generics.RetrieveAPIView):
    queryset = Municipality.objects.all()
    serializer_class = MunicipalityDetailSerializer

class MunicipalityList(generics.ListAPIView):
    queryset = Municipality.objects.all()
    serializer_class = MunicipalitySerializer