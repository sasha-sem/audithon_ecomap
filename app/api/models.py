from django.db import models
from django.contrib.gis.db import models as geomodels
from django.contrib.postgres.fields import JSONField
from django.core.validators import MaxValueValidator, MinValueValidator

class TestObj(models.Model):
    name = models.CharField(max_length=256, verbose_name='Название')
    geom = geomodels.MultiPolygonField(verbose_name='Геометрия',)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Тест'
        verbose_name = 'Тест'
class Regions(models.Model):
    name = models.CharField(max_length=256, verbose_name='Название')
    boundary = models.CharField(max_length=256, verbose_name='Граница')
    geom = geomodels.MultiPolygonField(verbose_name='Геометрия',)
    extent = geomodels.PolygonField(verbose_name='Extent',blank=True, null=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'Границы регионов'
        verbose_name = 'Границы регионов'
class Municipality(models.Model):
    name = models.CharField(max_length=256, verbose_name='Название')
    boundary = models.CharField(max_length=256, verbose_name='Граница')
    region = models.CharField(max_length=256, verbose_name='Регион')
    geom = geomodels.MultiPolygonField(verbose_name='Геометрия',)
    extent = geomodels.PolygonField(verbose_name='Extent',blank=True, null=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'Границы муниципалитетов'
        verbose_name = 'Границы муниципалитетов'
class Files(models.Model):
    title = models.CharField(max_length=300, unique=False, blank=False, null=True, verbose_name='Название',
                             db_index=True)
    file = models.FileField(upload_to='files', verbose_name='Файл',blank=False, null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Файлы'
        verbose_name = 'Файл'

#Экологически рискованные предприятия
class EcoRiskEntMun(models.Model):
    mun = models.OneToOneField(Municipality, on_delete=models.CASCADE, verbose_name='Муниципалитет',
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Экологически рискованные предприятия по муниципалитетам'
        verbose_name = 'Экологически рискованные предприятия'

class EcoRiskEntReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Экологически рискованные предприятия по регионам'
        verbose_name = 'Экологически рискованные предприятия'

#Очищенные загрязнения в воздухе
class PurAirPollutionMun(models.Model):
    mun = models.OneToOneField(Municipality, on_delete=models.CASCADE, verbose_name='Муниципалитет',
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Очищенные загрязнения в воздухе по муниципалитетам'
        verbose_name = 'Очищенные загрязнения в воздухе'

class PurAirPollutionReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Очищенные загрязнения в воздухе по регионами'
        verbose_name = 'Очищенные загрязнения в воздухе'

#Лесной покров занимает более 5%
class ForestsMun(models.Model):
    mun = models.OneToOneField(Municipality, on_delete=models.CASCADE, verbose_name='Муниципалитет',
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Лесной покров занимает более 5% муниципалитета'
        verbose_name = 'Лесной покров занимает более 5% муниципалитета'

class ForestsReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Количество муниципалитетов в регионе где лесной покров занимает более 5%'
        verbose_name = 'Количество муниципалитетов в регионе где лесной покров занимает более 5%'

#Количество ООПТ
class OOPTMun(models.Model):
    mun = models.OneToOneField(Municipality, on_delete=models.CASCADE, verbose_name='Муниципалитет',
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Количество ООПТ в муниципалитете'
        verbose_name = 'Количество ООПТ в муниципалитете'

class OOPTReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Количество ООПТ в регионе'
        verbose_name = 'Количество ООПТ в регионе'

#Загрязняющие вещества в воздухе
class AirPollutionMun(models.Model):
    mun = models.OneToOneField(Municipality, on_delete=models.CASCADE, verbose_name='Муниципалитет',
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Загрязняющие вещества в воздухе в муниципалитете'
        verbose_name = 'Загрязняющие вещества в воздухе в муниципалитете'

class AirPollutionReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Загрязняющие вещества в воздухе в регионе'
        verbose_name = 'Загрязняющие вещества в воздухе в регионе'

#Загрязняющие вещества
class ContaminantsMun(models.Model):
    mun = models.OneToOneField(Municipality, on_delete=models.CASCADE, verbose_name='Муниципалитет',
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Загрязняющие вещества в муниципалитете'
        verbose_name = 'Загрязняющие вещества в муниципалитете'

class ContaminantsReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Загрязняющие вещества в регионе'
        verbose_name = 'Загрязняющие вещества в регионе'

#Инвестиции
class AirProtectionInvReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Инвестиции в охрану атмосферного воздуха'
        verbose_name = 'Инвестиции в охрану атмосферного воздуха'

class EarthProtectionInvReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Инвестиции в охрану и рациональное использование земель'
        verbose_name = 'Инвестиции в охрану и рациональное использование земель'

class EnvProtectionInvReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Инвестиции в охрану окружающей среды и рациональное использование природных ресурсов'
        verbose_name = 'Инвестиции в охрану окружающей среды и рациональное использование природных ресурсов'

class LandReclamationInvReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Инвестиции в рекультивацию земель'
        verbose_name = 'Инвестиции в рекультивацию земель'

class ProtectionFromConsumptionInvReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Инвестиции в охрану окружающей среды от загрязнения отходами производства и потребления'
        verbose_name = 'Инвестиции в охрану окружающей среды от загрязнения отходами производства и потребления'

class WaterProtectionInvReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Инвестиции в охрану и рациональное использование водных ресурсов'
        verbose_name = 'Инвестиции в охрану и рациональное использование водных ресурсов'

#Инвестиции в Арктику
class ArcticInvestments(models.Model):
    env_protection = JSONField(verbose_name='Инвестициии в охрану окружающей среды и рациональное использование природных ресурсов', blank=True, null=True, )
    water_protection = JSONField(verbose_name='Инвестициии в охрану и рациональное использование водных ресурсов', blank=True, null=True, )
    air_protection = JSONField(verbose_name='Инвестициии в охрану атмосферного воздуха', blank=True, null=True, )
    earth_protection = JSONField(verbose_name='Инвестициии в охрану и рациональное использование земель', blank=True, null=True, )
    сonsumption_protection = JSONField(verbose_name='Инвестициии в охрану окружающей среды от вредного воздействия отходов производства и потребления', blank=True, null=True, )
    other_protection = JSONField(verbose_name='Прочие направления охраны', blank=True, null=True, )
    class Meta:
        verbose_name_plural = 'Общие инвестиции в арктику'
        verbose_name = 'Общие инвестиции в арктику'

#Затраты на Арктику
class ArcticCosts(models.Model):
    waste_management_costs = JSONField(verbose_name='Затраты на обращение с отходами', blank=True, null=True, )
    wastewater_costs = JSONField(verbose_name='Затраты на сбор и очистку сточных вод', blank=True, null=True, )
    air_protection_costs = JSONField(verbose_name='Затраты на охрану атмосферного воздуха и предотвращение изменения климата', blank=True, null=True, )
    radiation_safety_costs = JSONField(verbose_name='Затраты на обеспечение радиационной безопасности окружающей среды', blank=True, null=True, )
    rehabilitation_costs = JSONField(verbose_name='Затраты на защиту реабилитации земель, поверхностных и подземных вод', blank=True, null=True, )
    other_costs = JSONField(verbose_name='Затраты на другие направления деятельности в сфере охраны окружающей среды', blank=True, null=True, )
    class Meta:
        verbose_name_plural = 'Общие затраты на арктику'
        verbose_name = 'Общие затраты на арктику'

#Отходы
class WasteGenerationReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Образование отходов производства и потребления в регионах'
        verbose_name = 'Образование отходов производства и потребления в регионах'

class WasteRecyclingReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Утилизация отходов производства и потребления в регионах'
        verbose_name = 'Утилизация отходов производства и потребления в регионах'

class WasteDisposalReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Обезвреживание отходов производства и потребления в регионах'
        verbose_name = 'Обезвреживание отходов производства и потребления в регионах'

class WastePlacementReg(models.Model):
    reg = models.OneToOneField(Regions, on_delete=models.CASCADE, verbose_name='Регион', 
                               null=True,
                               blank=True)
    stats = JSONField(verbose_name='Статистика', blank=True, null=True, )

    class Meta:
        verbose_name_plural = 'Размещение на собственных объектах отходов производства и потребления в регионах'
        verbose_name = 'Размещение на собственных объектах отходов производства и потребления в регионах'