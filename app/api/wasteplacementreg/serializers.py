from rest_framework import serializers
from api.models import WastePlacementReg


class WastePlacementRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = WastePlacementReg
        fields = ('reg_id',
                  'stats',
                  )