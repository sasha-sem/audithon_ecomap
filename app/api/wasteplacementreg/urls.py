from django.urls import path
from .views import WastePlacementRegList

urlpatterns = [
    path('', WastePlacementRegList.as_view()),
]