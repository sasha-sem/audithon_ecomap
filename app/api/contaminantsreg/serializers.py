from rest_framework import serializers
from api.models import ContaminantsReg


class ContaminantsRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContaminantsReg
        fields = ('reg_id',
                  'stats',
                  )