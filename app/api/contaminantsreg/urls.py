from django.urls import path
from .views import  ContaminantsRegList

urlpatterns = [
    path('', ContaminantsRegList.as_view()),
]