from django.urls import path
from .views import  ForestsRegList

urlpatterns = [
    path('', ForestsRegList.as_view()),
]