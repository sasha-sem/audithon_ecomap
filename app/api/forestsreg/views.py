from django.shortcuts import render
from rest_framework import generics, status
from api.models import ForestsReg
from .serializers import ForestsRegSerializer
from rest_framework.response import Response


class ForestsRegList(generics.ListAPIView):
    queryset = ForestsReg.objects.all()
    serializer_class = ForestsRegSerializer
    def get(self, request):
        param_handler = self.request.query_params
        year = param_handler.get('year')
        region = param_handler.get('region')
        result = {"error":"incorrect input"}
        if region and year:
            try:
                data = ForestsReg.objects.filter(reg=region).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Количество муниципалитетов в регионе где лесной покров занимает более 5%",
                "units":"ед.",
                "year":year,
                "reg_id":data.reg_id,
                "reg_name":data.reg.name,
                "value":data.stats.get(str(year), None)
            }
        elif region:
            try:
                data = ForestsReg.objects.filter(reg=region).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Количество муниципалитетов в регионе где лесной покров занимает более 5%",
                "units":"ед.",
                "year":year,
                "reg_id":data.reg_id,
                "reg_name":data.reg.name,
                "stats": {
                    "years":data.stats.keys(),
                    "values": data.stats.values()
                }
            }
        elif year:
            data = ForestsReg.objects.all()
            result = {
                "title":"Количество муниципалитетов в регионе где лесной покров занимает более 5%",
                "units":"ед.",
                "year":year,
                "regions":[]
            }
            for it in data:
                result["regions"].append({"reg_id":it.reg_id,"reg_name":it.reg.name,  "value":it.stats.get(str(year), None)})

        return Response(result)