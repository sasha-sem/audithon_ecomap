from rest_framework import serializers
from api.models import ForestsReg


class ForestsRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = ForestsReg
        fields = ('reg_id',
                  'stats',
                  )