from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('testobj/', include('api.testobj.urls')),
    path('regions/', include('api.regions.urls')),
    path('munic/', include('api.municipality.urls')),
    path('ecoriskentreg/', include('api.ecoriskentreg.urls')),
    path('ecoriskentmun/', include('api.ecoriskentmun.urls')),
    path('purairpollutionmun/', include('api.purairpollutionmun.urls')),
    path('purairpollutionreg/', include('api.purairpollutionreg.urls')),
    path('purairpollutionreg/', include('api.purairpollutionreg.urls')),
    path('forestsmun/', include('api.forestsmun.urls')),
    path('forestsreg/', include('api.forestsreg.urls')),
    path('ooptmun/', include('api.ooptmun.urls')),
    path('ooptreg/', include('api.ooptreg.urls')),
    path('airpollutionmun/', include('api.airpollutionmun.urls')),
    path('airpollutionreg/', include('api.airpollutionreg.urls')),
    path('contaminantsmun/', include('api.contaminantsmun.urls')),
    path('contaminantsreg/', include('api.contaminantsreg.urls')),
    path('airprotectioninvreg/', include('api.airprotectioninvreg.urls')),
    path('earthprotectioninvreg/', include('api.earthprotectioninvreg.urls')),
    path('envprotectioninvreg/', include('api.envprotectioninvreg.urls')),
    path('landreclamationinvreg/', include('api.landreclamationinvreg.urls')),
    path('protectionfromconsumptioninvreg/', include('api.protectionfromconsumptioninvreg.urls')),
    path('waterprotectioninvreg/', include('api.waterprotectioninvreg.urls')),
    path('arcticinvestments/', include('api.arcticinvestments.urls')),
    path('arcticcosts/', include('api.arcticcosts.urls')),
    path('wastegenerationreg/', include('api.wastegenerationreg.urls')),
    path('wasterecyclingreg/', include('api.wasterecyclingreg.urls')),
    path('wastedisposalreg/', include('api.wastedisposalreg.urls')),
    path('wasteplacementreg/', include('api.wasteplacementreg.urls')),
]


urlpatterns = format_suffix_patterns(urlpatterns)