from rest_framework import serializers
from api.models import WasteRecyclingReg


class WasteRecyclingRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = WasteRecyclingReg
        fields = ('reg_id',
                  'stats',
                  )