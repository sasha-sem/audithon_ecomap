from django.urls import path
from .views import WasteRecyclingRegList

urlpatterns = [
    path('', WasteRecyclingRegList.as_view()),
]