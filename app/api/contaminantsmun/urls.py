from django.urls import path
from .views import  ContaminantsMunList

urlpatterns = [
    path('', ContaminantsMunList.as_view()),
]