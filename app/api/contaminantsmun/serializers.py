from rest_framework import serializers
from api.models import ContaminantsMun


class ContaminantsMunSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContaminantsMun
        fields = ('mun_id',
                  'stats',
                  )