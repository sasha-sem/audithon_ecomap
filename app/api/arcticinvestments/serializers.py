from rest_framework import serializers
from api.models import ArcticInvestments


class ArcticInvestmentsSerializer(serializers.ModelSerializer):
    plots = serializers.SerializerMethodField()
    def get_plots(self, instance):
        return [instance.env_protection, instance.water_protection, instance.air_protection,
                instance.earth_protection, instance.сonsumption_protection, instance.other_protection]

    class Meta:
        model = ArcticInvestments
        fields = ('plots',)