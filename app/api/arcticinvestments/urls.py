from django.urls import path
from .views import ArcticInvestmentsDetail

urlpatterns = [
    path('<int:pk>/', ArcticInvestmentsDetail.as_view()),
]