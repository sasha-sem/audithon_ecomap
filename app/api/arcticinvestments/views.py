from django.shortcuts import render
from rest_framework import generics, status
from api.models import ArcticInvestments
from .serializers import ArcticInvestmentsSerializer
# Create your views here.

class ArcticInvestmentsDetail(generics.RetrieveAPIView):
    queryset = ArcticInvestments.objects.all()
    serializer_class = ArcticInvestmentsSerializer
