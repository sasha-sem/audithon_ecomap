from django.shortcuts import render
from rest_framework import generics, status
from api.models import ArcticCosts
from .serializers import ArcticCostsSerializer
# Create your views here.

class ArcticCostsDetail(generics.RetrieveAPIView):
    queryset = ArcticCosts.objects.all()
    serializer_class = ArcticCostsSerializer
