from rest_framework import serializers
from api.models import ArcticCosts


class ArcticCostsSerializer(serializers.ModelSerializer):
    plots = serializers.SerializerMethodField()
    def get_plots(self, instance):
        return [instance.waste_management_costs, instance.wastewater_costs, instance.air_protection_costs,
                instance.radiation_safety_costs, instance.rehabilitation_costs, instance.other_costs]

    class Meta:
        model = ArcticCosts
        fields = ('plots',)