from django.urls import path
from .views import ArcticCostsDetail

urlpatterns = [
    path('<int:pk>/', ArcticCostsDetail.as_view()),
]