from rest_framework import serializers
from api.models import EarthProtectionInvReg


class EarthProtectionInvRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = EarthProtectionInvReg
        fields = ('reg_id',
                  'stats',
                  )