from django.shortcuts import render
from rest_framework import generics, status
from api.models import EarthProtectionInvReg
from .serializers import EarthProtectionInvRegSerializer
from rest_framework.response import Response


class EarthProtectionInvRegList(generics.ListAPIView):
    queryset = EarthProtectionInvReg.objects.all()
    serializer_class = EarthProtectionInvRegSerializer
    def get(self, request):
        param_handler = self.request.query_params
        year = param_handler.get('year')
        region = param_handler.get('region')
        result = {"error":"incorrect input"}
        if region and year:
            try:
                data = EarthProtectionInvReg.objects.filter(reg=region).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Инвестиции в охрану и рациональное использование земель",
                "units":"тыс. руб.",
                "year":year,
                "reg_id":data.reg_id,
                "reg_name":data.reg.name,
                "value":data.stats.get(str(year), None)
            }
        elif region:
            try:
                data = EarthProtectionInvReg.objects.filter(reg=region).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Инвестиции в охрану и рациональное использование земель",
                "units":"тыс. руб.",
                "year":year,
                "reg_id":data.reg_id,
                "reg_name":data.reg.name,
                "stats": {
                    "years":data.stats.keys(),
                    "values": data.stats.values()
                }
            }
        elif year:
            data = EarthProtectionInvReg.objects.all()
            result = {
                "title":"Инвестиции в охрану и рациональное использование земель",
                "units":"тыс. руб.",
                "year":year,
                "regions":[]
            }
            for it in data:
                result["regions"].append({"reg_id":it.reg_id,"reg_name":it.reg.name,  "value":it.stats.get(str(year), None)})

        return Response(result)