from django.urls import path
from .views import EarthProtectionInvRegList

urlpatterns = [
    path('', EarthProtectionInvRegList.as_view()),
]