from django.urls import path
from .views import  AirPollutionRegList

urlpatterns = [
    path('', AirPollutionRegList.as_view()),
]