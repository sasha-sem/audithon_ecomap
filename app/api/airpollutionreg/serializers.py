from rest_framework import serializers
from api.models import AirPollutionReg


class AirPollutionRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = AirPollutionReg
        fields = ('reg_id',
                  'stats',
                  )