# Generated by Django 2.2.19 on 2021-03-27 20:54

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_auto_20210327_2235'),
    ]

    operations = [
        migrations.CreateModel(
            name='WaterProtectionInvReg',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stats', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='Статистика')),
                ('reg', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Regions', verbose_name='Регион')),
            ],
            options={
                'verbose_name': 'Инвестиции в охрану и рациональное использование водных ресурсов',
                'verbose_name_plural': 'Инвестиции в охрану и рациональное использование водных ресурсов',
            },
        ),
        migrations.CreateModel(
            name='ProtectionFromConsumptionInvReg',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stats', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='Статистика')),
                ('reg', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Regions', verbose_name='Регион')),
            ],
            options={
                'verbose_name': 'Инвестиции в охрану окружающей среды от загрязнения отходами производства и потребления',
                'verbose_name_plural': 'Инвестиции в охрану окружающей среды от загрязнения отходами производства и потребления',
            },
        ),
        migrations.CreateModel(
            name='LandReclamationInvReg',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stats', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='Статистика')),
                ('reg', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Regions', verbose_name='Регион')),
            ],
            options={
                'verbose_name': 'Инвестиции в рекультивация земель',
                'verbose_name_plural': 'Инвестиции в рекультивация земель',
            },
        ),
        migrations.CreateModel(
            name='EnvProtectionInvReg',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stats', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='Статистика')),
                ('reg', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Regions', verbose_name='Регион')),
            ],
            options={
                'verbose_name': 'Инвестиции в охрану окружающей среды и рациональное использование природных ресурсов',
                'verbose_name_plural': 'Инвестиции в охрану окружающей среды и рациональное использование природных ресурсов',
            },
        ),
        migrations.CreateModel(
            name='EarthProtectionInvReg',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stats', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='Статистика')),
                ('reg', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Regions', verbose_name='Регион')),
            ],
            options={
                'verbose_name': 'Инвестиции в охрану и рациональное использование земель',
                'verbose_name_plural': 'Инвестиции в охрану и рациональное использование земель',
            },
        ),
        migrations.CreateModel(
            name='AirProtectionInvReg',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stats', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='Статистика')),
                ('reg', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Regions', verbose_name='Регион')),
            ],
            options={
                'verbose_name': 'Инвестиции в охрану атмосферного воздуха',
                'verbose_name_plural': 'Инвестиции в охрану атмосферного воздуха',
            },
        ),
    ]
