from django.urls import path
from .views import WaterProtectionInvRegList

urlpatterns = [
    path('', WaterProtectionInvRegList.as_view()),
]