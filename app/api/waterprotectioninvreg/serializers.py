from rest_framework import serializers
from api.models import WaterProtectionInvReg


class WaterProtectionInvRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = WaterProtectionInvReg
        fields = ('reg_id',
                  'stats',
                  )