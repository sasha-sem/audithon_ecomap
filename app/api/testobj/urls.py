from django.urls import path
from .views import TestObjDetail, TestObjList

urlpatterns = [
    path('<int:pk>/', TestObjDetail.as_view()),
    path('', TestObjList.as_view()),
]