from django.shortcuts import render
from rest_framework import generics, status
from api.models import TestObj
from .serializers import TestObjSerializer
# Create your views here.

class TestObjDetail(generics.RetrieveAPIView):
    queryset = TestObj.objects.all()
    serializer_class = TestObjSerializer

class TestObjList(generics.ListAPIView):
    queryset = TestObj.objects.all()
    serializer_class = TestObjSerializer