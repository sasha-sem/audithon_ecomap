from rest_framework import serializers
from api.models import TestObj


class TestObjSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestObj
        fields = '__all__'