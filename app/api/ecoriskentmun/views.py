from django.shortcuts import render
from rest_framework import generics, status
from api.models import EcoRiskEntMun
from .serializers import EcoRiskEntMunSerializer
from rest_framework.response import Response


class EcoRiskEntMunList(generics.ListAPIView):
    queryset = EcoRiskEntMun.objects.all()
    serializer_class = EcoRiskEntMunSerializer
    def get(self, request):
        param_handler = self.request.query_params
        year = param_handler.get('year')
        munic = param_handler.get('munic')
        result = {"error":"incorrect input"}
        if munic and year:
            try:
                data = EcoRiskEntMun.objects.filter(mun=munic).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Экологически рискованные предприятия",
                "units":"ед.",
                "year":year,
                "mun_id":data.mun_id,
                "mun_name":data.mun.name,
                "reg_name":data.mun.region,
                "value":data.stats.get(str(year), None)
            }
        elif munic:
            try:
                data = EcoRiskEntMun.objects.filter(mun=munic).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Экологически рискованные предприятия",
                "units":"ед.",
                "mun_id":data.mun_id,
                "mun_name":data.mun.name,
                "reg_name":data.mun.region,
                "stats": {
                    "years":data.stats.keys(),
                    "values": data.stats.values()
                }
            }
        elif year:
            data = EcoRiskEntMun.objects.all()
            result = {
                "title":"Экологически рискованные предприятия",
                "units":"ед.",
                "year":year,
                "regions":[]
            }
            for it in data:
                result["regions"].append({"mun_id":it.mun_id,"mun_name":it.mun.name, "reg_name":it.mun.region,  "value":it.stats.get(str(year), None)})

        return Response(result)