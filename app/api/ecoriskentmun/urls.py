from django.urls import path
from .views import  EcoRiskEntMunList

urlpatterns = [
    path('', EcoRiskEntMunList.as_view()),
]