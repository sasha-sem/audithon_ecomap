from rest_framework import serializers
from api.models import EcoRiskEntMun


class EcoRiskEntMunSerializer(serializers.ModelSerializer):
    class Meta:
        model = EcoRiskEntMun
        fields = ('mun_id',
                  'stats',
                  )