from django.shortcuts import render
from rest_framework import generics, status
from api.models import ProtectionFromConsumptionInvReg
from .serializers import ProtectionFromConsumptionInvRegSerializer
from rest_framework.response import Response


class ProtectionFromConsumptionInvRegList(generics.ListAPIView):
    queryset = ProtectionFromConsumptionInvReg.objects.all()
    serializer_class = ProtectionFromConsumptionInvRegSerializer
    def get(self, request):
        param_handler = self.request.query_params
        year = param_handler.get('year')
        region = param_handler.get('region')
        result = {"error":"incorrect input"}
        if region and year:
            try:
                data = ProtectionFromConsumptionInvReg.objects.filter(reg=region).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Инвестиции в охрану окружающей среды от загрязнения отходами производства и потребления",
                "units":"тыс. руб.",
                "year":year,
                "reg_id":data.reg_id,
                "reg_name":data.reg.name,
                "value":data.stats.get(str(year), None)
            }
        elif region:
            try:
                data = ProtectionFromConsumptionInvReg.objects.filter(reg=region).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Инвестиции в охрану окружающей среды от загрязнения отходами производства и потребления",
                "units":"тыс. руб.",
                "year":year,
                "reg_id":data.reg_id,
                "reg_name":data.reg.name,
                "stats": {
                    "years":data.stats.keys(),
                    "values": data.stats.values()
                }
            }
        elif year:
            data = ProtectionFromConsumptionInvReg.objects.all()
            result = {
                "title":"Инвестиции в охрану окружающей среды от загрязнения отходами производства и потребления",
                "units":"тыс. руб.",
                "year":year,
                "regions":[]
            }
            for it in data:
                result["regions"].append({"reg_id":it.reg_id,"reg_name":it.reg.name,  "value":it.stats.get(str(year), None)})

        return Response(result)