from django.urls import path
from .views import ProtectionFromConsumptionInvRegList

urlpatterns = [
    path('', ProtectionFromConsumptionInvRegList.as_view()),
]