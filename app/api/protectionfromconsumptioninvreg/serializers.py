from rest_framework import serializers
from api.models import ProtectionFromConsumptionInvReg


class ProtectionFromConsumptionInvRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProtectionFromConsumptionInvReg
        fields = ('reg_id',
                  'stats',
                  )