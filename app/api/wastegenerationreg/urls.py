from django.urls import path
from .views import WasteGenerationRegList

urlpatterns = [
    path('', WasteGenerationRegList.as_view()),
]