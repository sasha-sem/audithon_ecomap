from rest_framework import serializers
from api.models import WasteGenerationReg


class WasteGenerationRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = WasteGenerationReg
        fields = ('reg_id',
                  'stats',
                  )