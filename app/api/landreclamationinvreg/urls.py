from django.urls import path
from .views import LandReclamationInvRegList

urlpatterns = [
    path('', LandReclamationInvRegList.as_view()),
]