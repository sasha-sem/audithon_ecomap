from rest_framework import serializers
from api.models import LandReclamationInvReg


class LandReclamationInvRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = LandReclamationInvReg
        fields = ('reg_id',
                  'stats',
                  )