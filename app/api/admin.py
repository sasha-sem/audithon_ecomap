from django.contrib import admin
from django.contrib.gis import admin as gisadmin
from api.models import *
from django.utils.html import format_html

@admin.register(Municipality)
class MunicipalityAdmin(gisadmin.OSMGeoAdmin):
    list_display = ( "id", "name", "region")

@admin.register(Regions)
class RegionsAdmin(gisadmin.OSMGeoAdmin):
    list_display = ( "id", "name")

@admin.register(EcoRiskEntMun)
class EcoRiskEntMunAdmin(admin.ModelAdmin):
    list_display = ( "id", "mun")

@admin.register(EcoRiskEntReg)
class EcoRiskEntRegAdmin(admin.ModelAdmin):
    list_display = ( "id", "reg")

@admin.register(PurAirPollutionMun)
class PurAirPollutionMunAdmin(admin.ModelAdmin):
    list_display = ( "id", "mun")

@admin.register(PurAirPollutionReg)
class PurAirPollutionRegAdmin(admin.ModelAdmin):
    list_display = ( "id", "reg")

 
@admin.register(Files)
class FilesAdmin(admin.ModelAdmin):
    list_display = ( "id" ,"title", "get_url")
    def get_url(self, instance: Files):
        if instance.file:
            return format_html(
                '<a target="_blank" href="{}">'
                '{}'
                '</a>',
                instance.file.url,
                instance.file.url
            )
        return None
    get_url.short_description = 'Файл'