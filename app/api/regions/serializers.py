from rest_framework import serializers
from api.models import Regions


class RegionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Regions
        fields = '__all__'
        
class RegionsDetailSerializer(serializers.ModelSerializer):
    plots = serializers.SerializerMethodField()
    def get_plots(self, instance):
        result = []
        ecoriskent = {
            'ecoriskent':
            {'title': "Экологически рискованные предприятия",
                  "units": "ед.",
                  "stats": {
                      "years": instance.ecoriskentreg.stats.keys(),
                      "values": instance.ecoriskentreg.stats.values()
                  }}}
        purairpollution = {'purairpollution':{'title': "Очищенные загрязнения в воздухе", "units": "тыс. тонн",
                  "stats": {
                      "years": instance.purairpollutionreg.stats.keys(),
                      "values": instance.purairpollutionreg.stats.values()
                  }
                  }}
        forests = {'forests':{'title': "Количество муниципалитетов в регионе где лесной покров занимает более 5%", "units": "ед.",
                  "stats": {
                      "years": instance.forestsreg.stats.keys(),
                      "values": instance.forestsreg.stats.values()
                  }
                  }}
        oopt = {'oopt':{'title': "Количество ООПТ в регионе", "units": "ед.",
                  "stats": {
                      "years": instance.ooptreg.stats.keys(),
                      "values": instance.ooptreg.stats.values()
                  }
                  }}
        airpollution = {'airpollution':{'title': "Загрязняющие вещества в воздухе", "units": "тыс. тонн",
                  "stats": {
                      "years": instance.airpollutionreg.stats.keys(),
                      "values": instance.airpollutionreg.stats.values()
                  }
                  }}
        contaminants = {'contaminants':{'title': "Загрязняющие вещества", "units": "тыс. тонн",
                  "stats": {
                      "years": instance.contaminantsreg.stats.keys(),
                      "values": instance.contaminantsreg.stats.values()
                  }
                  }}
        
        airprotectioninv = {
            'airprotectioninv':
            {'title': "Инвестиции в охрану атмосферного воздуха",
                  "units": "тыс. руб.",
                  "stats": {
                      "years": instance.airprotectioninvreg.stats.keys(),
                      "values": instance.airprotectioninvreg.stats.values()
                  }}}
        earthprotectioninv = {'earthprotectioninv':{'title': "Инвестиции в охрану и рациональное использование земель", "units": "тыс. руб.",
                  "stats": {
                      "years": instance.earthprotectioninvreg.stats.keys(),
                      "values": instance.earthprotectioninvreg.stats.values()
                  }
                  }}
        envprotectioninv = {'envprotectioninv':{'title': "Инвестиции в охрану окружающей среды и рациональное использование природных ресурсов", "units": "тыс. руб.",
                  "stats": {
                      "years": instance.envprotectioninvreg.stats.keys(),
                      "values": instance.envprotectioninvreg.stats.values()
                  }
                  }}
        landreclamationinv = {'landreclamationinv':{'title': "Инвестиции в рекультивацию земель", "units": "тыс. руб.",
                  "stats": {
                      "years": instance.landreclamationinvreg.stats.keys(),
                      "values": instance.landreclamationinvreg.stats.values()
                  }
                  }}
        protectionfromconsumptioninv = {'protectionfromconsumptioninv':{'title': "Инвестиции в охрану окружающей среды от загрязнения отходами производства и потребления", "units": "тыс. руб.",
                  "stats": {
                      "years": instance.protectionfromconsumptioninvreg.stats.keys(),
                      "values": instance.protectionfromconsumptioninvreg.stats.values()
                  }
                  }}
        waterprotectioninv = {'waterprotectioninv':{'title': "Инвестиции в охрану и рациональное использование водных ресурсов", "units": "тыс. руб.",
                  "stats": {
                      "years": instance.waterprotectioninvreg.stats.keys(),
                      "values": instance.waterprotectioninvreg.stats.values()
                  }
                  }}
        wastegeneration = {'wastegeneration':{'title': "Образование отходов производства и потребления", "units": "тыс. тонн",
                  "stats": {
                      "years": instance.wastegenerationreg.stats.keys(),
                      "values": instance.wastegenerationreg.stats.values()
                  }
                  }}
        wasterecycling = {'wasterecycling':{'title': "Утилизация отходов производства и потребления", "units": "тыс. тонн",
                  "stats": {
                      "years": instance.wasterecyclingreg.stats.keys(),
                      "values": instance.wasterecyclingreg.stats.values()
                  }
                  }}
        wastedisposal = {'wastedisposal':{'title': "Обезвреживание отходов производства и потребления", "units": "тыс. тонн",
                  "stats": {
                      "years": instance.wastedisposalreg.stats.keys(),
                      "values": instance.wastedisposalreg.stats.values()
                  }
                  }}    
        wasteplacement = {'wasteplacement':{'title': "Обезвреживание отходов производства и потребления в регионах", "units": "тыс. тонн",
                  "stats": {
                      "years": instance.wasteplacementreg.stats.keys(),
                      "values": instance.wasteplacementreg.stats.values()
                  }
                  }}   
        result = [ecoriskent, purairpollution,forests, oopt,airpollution, contaminants,
                airprotectioninv, earthprotectioninv, envprotectioninv, landreclamationinv, 
                protectionfromconsumptioninv, waterprotectioninv, wastegeneration,
                wasterecycling, wastedisposal, wasteplacement]
        return result

    class Meta:
        model = Regions
        fields = ('id',
                  'name',
                  'plots',
                  'extent',
                  )