from django.shortcuts import render
from rest_framework import generics, status
from api.models import Regions
from .serializers import RegionsSerializer, RegionsDetailSerializer
# Create your views here.

class RegionsDetail(generics.RetrieveAPIView):
    queryset = Regions.objects.all()
    serializer_class = RegionsDetailSerializer

class RegionsList(generics.ListAPIView):
    queryset = Regions.objects.all()
    serializer_class = RegionsSerializer