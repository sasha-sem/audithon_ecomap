from django.urls import path
from .views import RegionsDetail, RegionsList

urlpatterns = [
    path('<int:pk>/', RegionsDetail.as_view()),
    path('', RegionsList.as_view()),
]