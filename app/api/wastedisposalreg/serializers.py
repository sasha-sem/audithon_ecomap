from rest_framework import serializers
from api.models import WasteDisposalReg


class WasteDisposalRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = WasteDisposalReg
        fields = ('reg_id',
                  'stats',
                  )