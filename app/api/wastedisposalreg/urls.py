from django.urls import path
from .views import WasteDisposalRegList

urlpatterns = [
    path('', WasteDisposalRegList.as_view()),
]