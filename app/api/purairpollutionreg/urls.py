from django.urls import path
from .views import  PurAirPollutionRegList

urlpatterns = [
    path('', PurAirPollutionRegList.as_view()),
]