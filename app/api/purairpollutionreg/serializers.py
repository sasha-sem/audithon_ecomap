from rest_framework import serializers
from api.models import PurAirPollutionReg


class PurAirPollutionRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = PurAirPollutionReg
        fields = ('reg_id',
                  'stats',
                  )