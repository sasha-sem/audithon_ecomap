from rest_framework import serializers
from api.models import OOPTReg


class OOPTRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = OOPTReg
        fields = ('reg_id',
                  'stats',
                  )