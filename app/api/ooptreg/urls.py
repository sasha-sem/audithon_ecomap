from django.urls import path
from .views import  OOPTRegList

urlpatterns = [
    path('', OOPTRegList.as_view()),
]