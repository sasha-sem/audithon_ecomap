from django.urls import path
from .views import  AirProtectionInvRegList

urlpatterns = [
    path('', AirProtectionInvRegList.as_view()),
]