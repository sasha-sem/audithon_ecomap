from rest_framework import serializers
from api.models import AirProtectionInvReg


class AirProtectionInvRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = AirProtectionInvReg
        fields = ('reg_id',
                  'stats',
                  )