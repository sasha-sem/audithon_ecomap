from django.shortcuts import render
from rest_framework import generics, status
from api.models import AirProtectionInvReg
from .serializers import AirProtectionInvRegSerializer
from rest_framework.response import Response


class AirProtectionInvRegList(generics.ListAPIView):
    queryset = AirProtectionInvReg.objects.all()
    serializer_class = AirProtectionInvRegSerializer
    def get(self, request):
        param_handler = self.request.query_params
        year = param_handler.get('year')
        region = param_handler.get('region')
        result = {"error":"incorrect input"}
        if region and year:
            try:
                data = AirProtectionInvReg.objects.filter(reg=region).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Инвестиции в охрану атмосферного воздуха",
                "units":"тыс. руб.",
                "year":year,
                "reg_id":data.reg_id,
                "reg_name":data.reg.name,
                "value":data.stats.get(str(year), None)
            }
        elif region:
            try:
                data = AirProtectionInvReg.objects.filter(reg=region).get()
            except:
                return Response({"error":"no region with this id"})
            result = {
                "title":"Инвестиции в охрану атмосферного воздуха",
                "units":"тыс. руб.",
                "year":year,
                "reg_id":data.reg_id,
                "reg_name":data.reg.name,
                "stats": {
                    "years":data.stats.keys(),
                    "values": data.stats.values()
                }
            }
        elif year:
            data = AirProtectionInvReg.objects.all()
            result = {
                "title":"Инвестиции в охрану атмосферного воздуха",
                "units":"тыс. руб.",
                "year":year,
                "regions":[]
            }
            for it in data:
                result["regions"].append({"reg_id":it.reg_id,"reg_name":it.reg.name,  "value":it.stats.get(str(year), None)})

        return Response(result)