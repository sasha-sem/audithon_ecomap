from django.urls import path
from .views import  EcoRiskEntRegList

urlpatterns = [
    path('', EcoRiskEntRegList.as_view()),
]