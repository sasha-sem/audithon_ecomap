from rest_framework import serializers
from api.models import EcoRiskEntReg


class EcoRiskEntRegSerializer(serializers.ModelSerializer):
    class Meta:
        model = EcoRiskEntReg
        fields = ('reg_id',
                  'stats',
                  )