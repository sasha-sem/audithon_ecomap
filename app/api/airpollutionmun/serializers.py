from rest_framework import serializers
from api.models import AirPollutionMun


class AirPollutionMunSerializer(serializers.ModelSerializer):
    class Meta:
        model = AirPollutionMun
        fields = ('mun_id',
                  'stats',
                  )