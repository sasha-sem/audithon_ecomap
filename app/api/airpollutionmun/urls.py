from django.urls import path
from .views import  AirPollutionMunList

urlpatterns = [
    path('', AirPollutionMunList.as_view()),
]